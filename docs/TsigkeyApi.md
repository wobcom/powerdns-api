# PowerdnsAPI.TsigkeyApi

All URIs are relative to *http://localhost:8081/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_tsig_key**](TsigkeyApi.md#create_tsig_key) | **POST** /servers/{server_id}/tsigkeys | Add a TSIG key
[**delete_tsig_key**](TsigkeyApi.md#delete_tsig_key) | **DELETE** /servers/{server_id}/tsigkeys/{tsigkey_id} | Delete the TSIGKey with tsigkey_id
[**get_tsig_key**](TsigkeyApi.md#get_tsig_key) | **GET** /servers/{server_id}/tsigkeys/{tsigkey_id} | Get a specific TSIGKeys on the server, including the actual key
[**list_tsig_keys**](TsigkeyApi.md#list_tsig_keys) | **GET** /servers/{server_id}/tsigkeys | Get all TSIGKeys on the server, except the actual key
[**put_tsig_key**](TsigkeyApi.md#put_tsig_key) | **PUT** /servers/{server_id}/tsigkeys/{tsigkey_id} | 


# **create_tsig_key**
> TSIGKey create_tsig_key(server_id, tsigkey)

Add a TSIG key

This methods add a new TSIGKey. The actual key can be generated by the server or be provided by the client

### Example

* Api Key Authentication (APIKeyHeader):
```python
from __future__ import print_function
import time
import PowerdnsAPI
from PowerdnsAPI.rest import ApiException
from pprint import pprint
configuration = PowerdnsAPI.Configuration()
# Configure API key authorization: APIKeyHeader
configuration.api_key['X-API-Key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-API-Key'] = 'Bearer'

# create an instance of the API class
api_instance = PowerdnsAPI.TsigkeyApi(PowerdnsAPI.ApiClient(configuration))
server_id = 'server_id_example' # str | The id of the server
tsigkey = PowerdnsAPI.TSIGKey() # TSIGKey | The TSIGKey to add

try:
    # Add a TSIG key
    api_response = api_instance.create_tsig_key(server_id, tsigkey)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TsigkeyApi->create_tsig_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **str**| The id of the server | 
 **tsigkey** | [**TSIGKey**](TSIGKey.md)| The TSIGKey to add | 

### Return type

[**TSIGKey**](TSIGKey.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |
**409** | Conflict. A key with this name already exists |  -  |
**422** | Unprocessable Entry, the TSIGKey provided has issues. |  -  |
**500** | Internal Server Error. There was a problem creating the key |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_tsig_key**
> delete_tsig_key(server_id, tsigkey_id)

Delete the TSIGKey with tsigkey_id

### Example

* Api Key Authentication (APIKeyHeader):
```python
from __future__ import print_function
import time
import PowerdnsAPI
from PowerdnsAPI.rest import ApiException
from pprint import pprint
configuration = PowerdnsAPI.Configuration()
# Configure API key authorization: APIKeyHeader
configuration.api_key['X-API-Key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-API-Key'] = 'Bearer'

# create an instance of the API class
api_instance = PowerdnsAPI.TsigkeyApi(PowerdnsAPI.ApiClient(configuration))
server_id = 'server_id_example' # str | The id of the server to retrieve the key from
tsigkey_id = 'tsigkey_id_example' # str | The id of the TSIGkey. Should match the \"id\" field in the TSIGKey object

try:
    # Delete the TSIGKey with tsigkey_id
    api_instance.delete_tsig_key(server_id, tsigkey_id)
except ApiException as e:
    print("Exception when calling TsigkeyApi->delete_tsig_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **str**| The id of the server to retrieve the key from | 
 **tsigkey_id** | **str**| The id of the TSIGkey. Should match the \&quot;id\&quot; field in the TSIGKey object | 

### Return type

void (empty response body)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | OK, key was deleted |  -  |
**404** | Not found. The TSIGKey with the specified tsigkey_id does not exist |  -  |
**500** | Internal Server Error. Contains error message |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tsig_key**
> TSIGKey get_tsig_key(server_id, tsigkey_id)

Get a specific TSIGKeys on the server, including the actual key

### Example

* Api Key Authentication (APIKeyHeader):
```python
from __future__ import print_function
import time
import PowerdnsAPI
from PowerdnsAPI.rest import ApiException
from pprint import pprint
configuration = PowerdnsAPI.Configuration()
# Configure API key authorization: APIKeyHeader
configuration.api_key['X-API-Key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-API-Key'] = 'Bearer'

# create an instance of the API class
api_instance = PowerdnsAPI.TsigkeyApi(PowerdnsAPI.ApiClient(configuration))
server_id = 'server_id_example' # str | The id of the server to retrieve the key from
tsigkey_id = 'tsigkey_id_example' # str | The id of the TSIGkey. Should match the \"id\" field in the TSIGKey object

try:
    # Get a specific TSIGKeys on the server, including the actual key
    api_response = api_instance.get_tsig_key(server_id, tsigkey_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TsigkeyApi->get_tsig_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **str**| The id of the server to retrieve the key from | 
 **tsigkey_id** | **str**| The id of the TSIGkey. Should match the \&quot;id\&quot; field in the TSIGKey object | 

### Return type

[**TSIGKey**](TSIGKey.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK. |  -  |
**404** | Not found. The TSIGKey with the specified tsigkey_id does not exist |  -  |
**500** | Internal Server Error, keys could not be retrieved. Contains error message |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_tsig_keys**
> list[TSIGKey] list_tsig_keys(server_id)

Get all TSIGKeys on the server, except the actual key

### Example

* Api Key Authentication (APIKeyHeader):
```python
from __future__ import print_function
import time
import PowerdnsAPI
from PowerdnsAPI.rest import ApiException
from pprint import pprint
configuration = PowerdnsAPI.Configuration()
# Configure API key authorization: APIKeyHeader
configuration.api_key['X-API-Key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-API-Key'] = 'Bearer'

# create an instance of the API class
api_instance = PowerdnsAPI.TsigkeyApi(PowerdnsAPI.ApiClient(configuration))
server_id = 'server_id_example' # str | The id of the server

try:
    # Get all TSIGKeys on the server, except the actual key
    api_response = api_instance.list_tsig_keys(server_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TsigkeyApi->list_tsig_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **str**| The id of the server | 

### Return type

[**list[TSIGKey]**](TSIGKey.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | List of TSIGKey objects |  -  |
**500** | Internal Server Error, keys could not be retrieved. Contains error message |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_tsig_key**
> TSIGKey put_tsig_key(server_id, tsigkey_id, tsigkey)



The TSIGKey at tsigkey_id can be changed in multiple ways:  * Changing the Name, this will remove the key with tsigkey_id after adding.  * Changing the Algorithm  * Changing the Key Only the relevant fields have to be provided in the request body. 

### Example

* Api Key Authentication (APIKeyHeader):
```python
from __future__ import print_function
import time
import PowerdnsAPI
from PowerdnsAPI.rest import ApiException
from pprint import pprint
configuration = PowerdnsAPI.Configuration()
# Configure API key authorization: APIKeyHeader
configuration.api_key['X-API-Key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-API-Key'] = 'Bearer'

# create an instance of the API class
api_instance = PowerdnsAPI.TsigkeyApi(PowerdnsAPI.ApiClient(configuration))
server_id = 'server_id_example' # str | The id of the server to retrieve the key from
tsigkey_id = 'tsigkey_id_example' # str | The id of the TSIGkey. Should match the \"id\" field in the TSIGKey object
tsigkey = PowerdnsAPI.TSIGKey() # TSIGKey | A (possibly stripped down) TSIGKey object with the new values

try:
    api_response = api_instance.put_tsig_key(server_id, tsigkey_id, tsigkey)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TsigkeyApi->put_tsig_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **str**| The id of the server to retrieve the key from | 
 **tsigkey_id** | **str**| The id of the TSIGkey. Should match the \&quot;id\&quot; field in the TSIGKey object | 
 **tsigkey** | [**TSIGKey**](TSIGKey.md)| A (possibly stripped down) TSIGKey object with the new values | 

### Return type

[**TSIGKey**](TSIGKey.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK. TSIGKey is changed. |  -  |
**404** | Not found. The TSIGKey with the specified tsigkey_id does not exist |  -  |
**500** | Internal Server Error. Contains error message |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

