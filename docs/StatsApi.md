# PowerdnsAPI.StatsApi

All URIs are relative to *http://localhost:8081/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_stats**](StatsApi.md#get_stats) | **GET** /servers/{server_id}/statistics | Query statistics.


# **get_stats**
> list[object] get_stats(server_id, statistic=statistic)

Query statistics.

Query PowerDNS internal statistics.

### Example

* Api Key Authentication (APIKeyHeader):
```python
from __future__ import print_function
import time
import PowerdnsAPI
from PowerdnsAPI.rest import ApiException
from pprint import pprint
configuration = PowerdnsAPI.Configuration()
# Configure API key authorization: APIKeyHeader
configuration.api_key['X-API-Key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['X-API-Key'] = 'Bearer'

# create an instance of the API class
api_instance = PowerdnsAPI.StatsApi(PowerdnsAPI.ApiClient(configuration))
server_id = 'server_id_example' # str | The id of the server to retrieve
statistic = 'statistic_example' # str | When set to the name of a specific statistic, only this value is returned. If no statistic with that name exists, the response has a 422 status and an error message.  (optional)

try:
    # Query statistics.
    api_response = api_instance.get_stats(server_id, statistic=statistic)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StatsApi->get_stats: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **server_id** | **str**| The id of the server to retrieve | 
 **statistic** | **str**| When set to the name of a specific statistic, only this value is returned. If no statistic with that name exists, the response has a 422 status and an error message.  | [optional] 

### Return type

**list[object]**

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | List of Statistic Items |  -  |
**422** | Returned when a non-existing statistic name has been requested. Contains an error message |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

