# coding: utf-8

"""
    PowerDNS Authoritative HTTP API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 0.0.13
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import PowerdnsAPI
from PowerdnsAPI.models.tsig_key import TSIGKey  # noqa: E501
from PowerdnsAPI.rest import ApiException


class TestTSIGKey(unittest.TestCase):
    """TSIGKey unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testTSIGKey(self):
        """Test TSIGKey"""
        # FIXME: construct object with mandatory attributes with example values
        # model = PowerdnsAPI.models.tsig_key.TSIGKey()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
