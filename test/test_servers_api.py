# coding: utf-8

"""
    PowerDNS Authoritative HTTP API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 0.0.13
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import PowerdnsAPI
from PowerdnsAPI.api.servers_api import ServersApi  # noqa: E501
from PowerdnsAPI.rest import ApiException


class TestServersApi(unittest.TestCase):
    """ServersApi unit test stubs"""

    def setUp(self):
        self.api = PowerdnsAPI.api.servers_api.ServersApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_cache_flush_by_name(self):
        """Test case for cache_flush_by_name

        Flush a cache-entry by name  # noqa: E501
        """
        pass

    def test_list_server(self):
        """Test case for list_server

        List a server  # noqa: E501
        """
        pass

    def test_list_servers(self):
        """Test case for list_servers

        List all servers  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
