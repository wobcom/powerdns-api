# coding: utf-8

"""
    PowerDNS Authoritative HTTP API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 0.0.13
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six


class Metadata(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'kind': 'str',
        'metadata': 'list[str]'
    }

    attribute_map = {
        'kind': 'kind',
        'metadata': 'metadata'
    }

    def __init__(self, kind=None, metadata=None):  # noqa: E501
        """Metadata - a model defined in OpenAPI"""  # noqa: E501

        self._kind = None
        self._metadata = None
        self.discriminator = None

        if kind is not None:
            self.kind = kind
        if metadata is not None:
            self.metadata = metadata

    @property
    def kind(self):
        """Gets the kind of this Metadata.  # noqa: E501

        Name of the metadata  # noqa: E501

        :return: The kind of this Metadata.  # noqa: E501
        :rtype: str
        """
        return self._kind

    @kind.setter
    def kind(self, kind):
        """Sets the kind of this Metadata.

        Name of the metadata  # noqa: E501

        :param kind: The kind of this Metadata.  # noqa: E501
        :type: str
        """

        self._kind = kind

    @property
    def metadata(self):
        """Gets the metadata of this Metadata.  # noqa: E501

        Array with all values for this metadata kind.  # noqa: E501

        :return: The metadata of this Metadata.  # noqa: E501
        :rtype: list[str]
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this Metadata.

        Array with all values for this metadata kind.  # noqa: E501

        :param metadata: The metadata of this Metadata.  # noqa: E501
        :type: list[str]
        """

        self._metadata = metadata

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Metadata):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
